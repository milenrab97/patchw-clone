import { range, transpose } from "ramda"

export const reverse = matrix => matrix.map(row => row.reverse())

export const rotateClockWise = matrix => reverse(transpose(matrix))

export const rotateCounterClockWise = matrix =>
  range(0, 3).reduce(acc => rotateClockWise(acc), matrix)
