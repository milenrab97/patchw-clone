import React from "react"
import { useDrag } from "react-dnd"

export const Piece = ({ className, id, matrix }) => {
  const [{ isDragging }, drag] = useDrag({
    item: { type: "piece", id, matrix },
    collect: monitor => ({
      isDragging: !!monitor.isDragging()
    })
  })

  return (
    <div
      className={className}
      ref={drag}
      style={{
        opacity: isDragging ? 0.5 : 1,
        fontSize: 25,
        fontWeight: "bold",
        cursor: "move"
      }}
    >
      {matrix.map((row, i) => (
        <div key={i} className="d-flex">
          {row.map((cell, j) => (
            <div key={j}>
              <div className={`piece-cell ${!cell ? "empty-piece" : ""}`} />
            </div>
          ))}
        </div>
      ))}
    </div>
  )
}
