import React from "react"
import { useDrop } from "react-dnd"
import { GameContext } from "../App"
import classNames from "classnames"

const BoardSquare = ({
  checkIfNearHovered,
  setHovered,
  canDropPiece,
  handleDrop,
  filled,
  accepting,
  x,
  y
}) => {
  const [{ draggedItem, isOver, canDrop }, drop] = useDrop({
    accept: "piece",
    drop: item => handleDrop(x, y, item),
    canDrop: () => canDropPiece(x, y, draggedItem),
    collect: monitor => ({
      isOver: !!monitor.isOver(),
      canDrop: !!monitor.canDrop(),
      draggedItem: monitor.getItem()
    })
  })

  const ctx = React.useContext(GameContext)
  React.useEffect(() => {
    if (isOver) {
      ctx.setValidMove(canDrop)
    }
  }, [isOver, ctx, canDrop])

  React.useEffect(() => {
    if (isOver) {
      setHovered({ x, y })
    } else {
      setHovered(null)
    }
  }, [x, y, isOver, setHovered])

  return (
    <div
      ref={drop}
      className={classNames("board-cell", {
        "board-piece-filled": filled,
        "valid-move-cell":
          ctx.validMove && checkIfNearHovered(x, y, draggedItem),
        "cannot-drop": !ctx.validMove && checkIfNearHovered(x, y, draggedItem)
      })}
    />
  )
}

export const Board = ({
  checkIfNearHovered,
  setHovered,
  boardMatrix,
  handleDrop,
  canDropPiece
}) => {
  return (
    <div className="board">
      {boardMatrix.map((row, i) => (
        <div key={i} className="d-flex">
          {row.map((cell, j) => (
            <div key={j}>
              <BoardSquare
                checkIfNearHovered={checkIfNearHovered}
                canDropPiece={canDropPiece}
                handleDrop={handleDrop}
                {...cell}
                setHovered={setHovered}
                x={i}
                y={j}
              />
            </div>
          ))}
        </div>
      ))}
    </div>
  )
}
