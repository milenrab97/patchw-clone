import React from "react"
import "./App.css"
import { Board } from "./components/Board"
import { Row, Col } from "reactstrap"
import { Piece } from "./components/Piece"
import { DndProvider } from "react-dnd"
import { HTML5Backend } from "react-dnd-html5-backend"
import { range } from "ramda"
import { rotateClockWise, rotateCounterClockWise } from "./utils/utils"

const T_matrix = [
  [1, 0],
  [1, 1],
  [1, 0]
]

const U_matrix = [
  [1, 0, 1],
  [1, 0, 1],
  [1, 1, 1]
]

const addPieceToBoard = (piece, board, toX, toY) => {
  const cpyBoard = JSON.parse(JSON.stringify(board))

  piece.forEach((row, i) => {
    row.forEach((cell, j) => {
      if (cell) {
        const currBoardState = cpyBoard[toX + i][toY + j]
        cpyBoard[toX + i][toY + j] = {
          ...currBoardState,
          filled: true
        }
      }
    })
  })

  return cpyBoard
}

const checkIfCanDrop = (piece, board, toX, toY) => {
  let can = true
  piece.forEach((row, i) => {
    row.forEach((cell, j) => {
      if (
        cell &&
        (!board[toX + i] ||
          !board[toX + i][toY + j] ||
          board[toX + i][toY + j].filled)
      ) {
        can = false
      }
    })
  })

  return can
}

const checkIfHoverdIsNearbyF = (piece, board, toX, toY, hovered) => {
  if (!hovered) {
    return false
  }

  const { x, y } = hovered
  let nearby = false
  piece.forEach((row, i) => {
    row.forEach((cell, j) => {
      if (cell && x + i === toX && y + j === toY) {
        nearby = true
      }
    })
  })

  return nearby
}

export const GameContext = React.createContext({ validMove: true })

const initialPieces = [
  { id: 1, matrix: T_matrix },
  { id: 2, matrix: U_matrix },
  { id: 3, matrix: rotateClockWise(U_matrix) },
  { id: 4, matrix: rotateClockWise(T_matrix) },
  { id: 6, matrix: rotateCounterClockWise(U_matrix) }
]

function App() {
  const [pieces, setPieces] = React.useState(initialPieces)

  const [boardMatrix, setBoardMatrix] = React.useState(
    range(0, 9).map(_ =>
      range(0, 9).map(c => ({ filled: false, accepting: false }))
    )
  )

  const [hovered, setHovered] = React.useState(null)

  const checkIfNearHovered = (x, y, item) => {
    return (
      item && checkIfHoverdIsNearbyF(item.matrix, boardMatrix, x, y, hovered)
    )
  }

  const handleDrop = (x, y, item) => {
    if (item) {
      setBoardMatrix(addPieceToBoard(item.matrix, boardMatrix, x, y))
      setPieces(pieces.filter(p => p.id !== item.id))
    }
  }

  const canDropPiece = React.useCallback(
    (x, y, item) => {
      return item && checkIfCanDrop(item.matrix, boardMatrix, x, y)
    },
    [boardMatrix]
  )

  const [validMove, setValidMove] = React.useState(true)

  return (
    <div className="App">
      <GameContext.Provider value={{ validMove, setValidMove }}>
        <DndProvider backend={HTML5Backend}>
          <Row className="mt-2 mb-5">
            <Col className="d-flex justify-content-center">
              <Board
                checkIfNearHovered={checkIfNearHovered}
                setHovered={setHovered}
                canDropPiece={canDropPiece}
                handleDrop={handleDrop}
                boardMatrix={boardMatrix}
              />
            </Col>
          </Row>
          <Row>
            <Col className="d-flex justify-content-center">
              {pieces.map(p => (
                <Piece
                  className="mr-4"
                  key={p.id}
                  id={p.id}
                  matrix={p.matrix}
                />
              ))}
            </Col>
          </Row>
        </DndProvider>
      </GameContext.Provider>
    </div>
  )
}

export default App
